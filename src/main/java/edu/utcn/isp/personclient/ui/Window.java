package edu.utcn.isp.personclient.ui;

import edu.utcn.isp.personclient.dto.PersonDTO;
import edu.utcn.isp.personclient.repo.PersonRepository;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author radumiron
 * @since 01.04.2019
 */
public class Window extends JFrame {
    private PersonRepository personRepository;

    private JTextField idInput;
    private JTextField firstNameInput;
    private JTextField lastNameInput;
    private JTextArea console;

    public Window(String title, PersonRepository personRepository) {
        super(title);

        this.personRepository = personRepository;
        this.setLayout(null);
        this.setSize(500, 500);

        JLabel idLabel = new JLabel("ID");
        idLabel.setBounds(20, 20, 300, 20);
        this.add(idLabel);

        idInput = new JTextField();
        idInput.setBounds(120, 20, 300, 20);
        this.add(idInput);

        JLabel firstNameLabel = new JLabel("First Name");
        firstNameLabel.setBounds(20, 45, 300, 20);
        this.add(firstNameLabel);

        firstNameInput = new JTextField();
        firstNameInput.setBounds(120, 45, 300, 20);
        this.add(firstNameInput);

        JLabel lastNameLabel = new JLabel("Last Name");
        lastNameLabel.setBounds(20, 70, 300, 20);
        this.add(lastNameLabel);

        lastNameInput = new JTextField();
        lastNameInput.setBounds(120, 70, 300, 20);
        this.add(lastNameInput);

        JButton createButton = new JButton("Create");
        createButton.setBounds(20, 95, 100, 20);
        this.add(createButton);
        createButton.addActionListener(
                e -> {
                    String idNumber = idInput.getText();
                    String firstName = firstNameInput.getText();
                    String lastName = lastNameInput.getText();
                    PersonDTO personEntity = new PersonDTO(idNumber, firstName, lastName);

                    if (personRepository.create(personEntity)) {
                        console.setText("Successfully created \n" + personEntity.toString());
                    } else {
                        console.setText("An error has occurred");
                    }
                }
        );

        JButton readButton = new JButton("Read");
        readButton.setBounds(130, 95, 100, 20);
        this.add(readButton);
        readButton.addActionListener(new ReadHandler());

        JButton updateButton = new JButton("Update");
        updateButton.setBounds(240, 95, 100, 20);
        this.add(updateButton);
        updateButton.addActionListener(new UpdateHandler());

        JButton deleteButton = new JButton("Delete");
        deleteButton.setBounds(350, 95, 100, 20);
        this.add(deleteButton);
        deleteButton.addActionListener(new DeleteHandler());

        console = new JTextArea();
        console.setBounds(20, 120, 450, 300);
        this.add(console);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

//    private class CreateHandler implements ActionListener {
//        @Override
//        public void actionPerformed(ActionEvent e) {
//            String idNumber = idInput.getText();
//            String firstName = firstNameInput.getText();
//            String lastName = lastNameInput.getText();
//            PersonEntity personEntity = new PersonEntity(idNumber, firstName, lastName);
//            personRepository.create(personEntity);
//            console.setText("Successfully created \n" + personEntity.toString());
//        }
//    }

    private class ReadHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String idNumber = idInput.getText();
            PersonDTO personEntity = personRepository.read(idNumber);
            console.setText(personEntity.toString());
        }
    }

    private class UpdateHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String idNumber = idInput.getText();
            String firstName = firstNameInput.getText();
            String lastName = lastNameInput.getText();
            PersonDTO personEntity = new PersonDTO(idNumber, firstName, lastName);

            if (personRepository.update(personEntity)) {
                console.setText("Successfully updated \n" + personEntity.toString());
            } else {
                console.setText("An error has occurred");
            }
        }
    }

    private class DeleteHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String idNumber = idInput.getText();

            if (personRepository.delete(idNumber)) {
                console.setText("Successfully deleted person with id=" + idNumber + "\n");
            } else {
                console.setText("An error has occurred");
            }
        }
    }
}
