package edu.utcn.isp.personclient.repo;


import edu.utcn.isp.personclient.dto.PersonDTO;

/**
 * @author radumiron
 * @since 6/2/21
 */
public interface PersonRepository {
    boolean create(PersonDTO personEntity);

    PersonDTO read(String idNumber);

    boolean update(PersonDTO personEntity);

    boolean delete(String idNumber);
}
