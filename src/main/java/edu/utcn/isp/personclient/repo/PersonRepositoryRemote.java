package edu.utcn.isp.personclient.repo;

import edu.utcn.isp.personclient.dto.PersonDTO;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author radumiron
 * @since 6/2/21
 */
public class PersonRepositoryRemote implements PersonRepository {

    private static final String REST_URI = "http://localhost:8080/persons";
    private Client client = ClientBuilder.newClient();

    @Override
    public boolean create(PersonDTO personDTO) {
        Response response = client.target(REST_URI)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(personDTO, MediaType.APPLICATION_JSON));

        return parseResponse(response);
    }

    @Override
    public PersonDTO read(String idNumber) {
        return client.target(REST_URI)
                .path(idNumber)
                .request(MediaType.APPLICATION_JSON)
                .get(PersonDTO.class);
    }

    @Override
    public boolean update(PersonDTO personDTO) {
        Response response = client.target(REST_URI)
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.entity(personDTO, MediaType.APPLICATION_JSON));
        return parseResponse(response);
    }

    @Override
    public boolean delete(String idNumber) {
        Response response = client.target(REST_URI)
                .path(idNumber)
                .request(MediaType.APPLICATION_JSON)
                .delete();

        return parseResponse(response);
    }

    private boolean parseResponse(Response response) {
        if (response.getStatus() == 200) {
            System.out.println("Success");
            return true;
        } else {
            System.err.println("An error has occurred");
            return false;
        }
    }
}
