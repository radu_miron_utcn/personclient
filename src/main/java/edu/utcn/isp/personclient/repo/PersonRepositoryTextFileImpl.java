package edu.utcn.isp.personclient.repo;


import edu.utcn.isp.personclient.dto.PersonDTO;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author radumiron
 * @since 6/2/21
 */
// TODO implement the rest of the methods
public class PersonRepositoryTextFileImpl implements PersonRepository {
    private static final String REPO_DIR_ABSOLUTE_PATH = "/tmp/person-repo-dir";
    private Path repoDir;

    public PersonRepositoryTextFileImpl() {
        try {
            this.repoDir = Files.exists(Paths.get(REPO_DIR_ABSOLUTE_PATH)) ?
                    Paths.get(REPO_DIR_ABSOLUTE_PATH) :
                    Files.createDirectory(Paths.get(REPO_DIR_ABSOLUTE_PATH));
        } catch (IOException e) {
            throw new RuntimeException("Unable to create the repo dir", e);
        }
    }

    @Override
    public boolean create(PersonDTO personEntity) {
        try (FileWriter fileWriter = new FileWriter(repoDir.resolve(personEntity.getIdNumber()).toFile())) {
            fileWriter.write(personEntity.toString());
            return true;
        } catch (IOException e) {
            e.printStackTrace(); // you might wanna show this in the UI
            return false;
        }
    }

    @Override
    public PersonDTO read(String idNumber) {
        return null; // hint use .substring() to deserialize
    }

    @Override
    public boolean update(PersonDTO personEntity) {
        // todo implement
        return true;
    }

    @Override
    public boolean delete(String idNumber) {
        // todo implement
        return true;
    }
}
