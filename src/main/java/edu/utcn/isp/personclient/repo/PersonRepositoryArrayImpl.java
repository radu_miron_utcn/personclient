package edu.utcn.isp.personclient.repo;


import edu.utcn.isp.personclient.dto.PersonDTO;

/**
 * @author radumiron
 * @since 6/2/21
 */
public class PersonRepositoryArrayImpl implements PersonRepository {
    private PersonDTO[] personEntities = new PersonDTO[500];
    private int counter = 0;

    public boolean create(PersonDTO personEntity) {
        personEntities[counter] = personEntity;
        counter++;
        return true;
    }

    public PersonDTO read(String idNumber) {
        for (PersonDTO personEntity : personEntities) {
            if (idNumber.equals(personEntity.getIdNumber())) {
                return personEntity;
            }
        }

        return null;
    }

    public boolean update(PersonDTO personEntity) {
        //todo implement
        return true;
    }

    public boolean delete(String idNumber) {
        //todo implement
        return true;
    }
}
