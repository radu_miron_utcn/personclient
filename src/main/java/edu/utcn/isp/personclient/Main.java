package edu.utcn.isp.personclient;

import edu.utcn.isp.personclient.repo.PersonRepository;
import edu.utcn.isp.personclient.ui.Window;

/**
 * @author radumiron
 * @since 6/2/21
 */
public class Main {
    public static void main(String[] args) {

        if (args.length != 1) {
            throw new IllegalArgumentException("Please specify the repository implementation class name");
        }

        PersonRepository personRepository;

        try {
            // we'll use reflexion to instantiate one edu.utcn.isp.repo.PersonRepository implementation
            String repoPackage = PersonRepository.class.getPackage().getName();
            String repoClassCanonicalName = repoPackage + "." + args[0];
            personRepository = (PersonRepository) Class.forName(repoClassCanonicalName).getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new IllegalArgumentException("The chosen repository implementation doesn't exist");
        }

        new Window("Person CRUD", personRepository);
    }
}
